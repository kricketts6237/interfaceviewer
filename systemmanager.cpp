#include "systemmanager.hpp"

int SystemManager::populate() {
	// Populate the namespaces
	int err = this->ns_manager.populate();
	
	if (err != 0)
		return err;
	
	// For each namespace, enter it, enumerate the interfaces and leave
	std::vector<NetNS *> ns_list = this->ns_manager.getNamespaces();
	for (unsigned int i = 0; i < ns_list.size(); i++) {
		if ((err = this->ns_manager.enter(ns_list[i]))) {
			return err;
		}
		
		IFaceManager_ptr mgr(new IFaceManager());
		
		if ((err = mgr->populate())) {
			return err;
		}
		
		this->iface_managers[ns_list[i]->name] = std::move(mgr);
		
		if ((err = this->ns_manager.leave())) {
			return err;
		}
	}
	
	return 0;
}


std::vector<NetNS *> SystemManager::getNamespaces() {
	return this->ns_manager.getNamespaces();
}

std::vector<IFaceManager *> SystemManager::getIFaceManagers() {
	std::vector<IFaceManager *> ret;
	
	for (auto it = this->iface_managers.begin(); it != iface_managers.end(); it++) {
		ret.push_back(it->second.get());
	}
	
	return ret;
}

// Get a list of all interfaces in all namespaces
// You may get interfaces of the same name in different namespaces
// Particularly true for the loopback interface
std::vector<IFace *> SystemManager::getInterfaces() {
	std::vector<IFace *> ret;
	
	for (IFaceManager *mgr : this->getIFaceManagers()) {
		for (IFace *iface : mgr->getInterfaces()) {
			ret.push_back(iface);
		}
	}
	
	return ret;
}

IFace *SystemManager::getInterfaceByIndex(int idx) {
	std::vector<IFace *> ifaces = this->getInterfaces();
	
	for (IFace *iface : ifaces) {
		if (iface->index == idx)
			return iface;
	}
	
	return nullptr;
}

int SystemManager::getInterfacesInNamespace(NetNS *ns, std::vector<IFace *> *ret) {
	if (this->iface_managers.count(ns->name) == 0)
		return EINVAL;
	
	IFaceManager *mgr = this->iface_managers[ns->name].get();
	for (IFace *iface : mgr->getInterfaces()) {
		ret->push_back(iface);
	}
	
	
	return 0;
}


// Return the namespace an interface resides in
NetNS *SystemManager::getNamespaceForInterface(IFace *iface) {
	for (auto it = this->iface_managers.begin(); it != iface_managers.end(); it++) {
		if (it->second->containsInterface(iface)) {
			return this->ns_manager.getNamespaceByName(it->first);
		}
	}
	
	return nullptr;
}
