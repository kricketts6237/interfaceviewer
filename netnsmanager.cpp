#include "netnsmanager.hpp"

NetNSManager::NetNSManager() {
	
}

NetNSManager::~NetNSManager() {
	for (unsigned int i = 0; i < this->namespaces.size(); i++) {
		if (this->namespaces[i]->handle != 0) {
			this->namespaces[i]->finish();
		}
	}
}

int NetNSManager::populate() {
	// Close any open namespace instances
	for (unsigned int i = 0; i < this->namespaces.size(); i++) {
		if (this->namespaces[i]->handle != 0) {
			int err = this->namespaces[i]->finish();
			if (err != 0)
				return errno;
		}
	}
	
	this->namespaces.clear();
	
	// Populate the list of namespaces
	NetNS_ptr self(new NetNS());
	self->name = this->home_ns;
	self->path = NETNS_SELF;
	
	this->namespaces.push_back(std::move(self));
	
	DIR *dp = opendir(NETNS_PATH);
	
	struct dirent *ep;
	
	if (dp != NULL) {
		while ((ep = readdir(dp)) != NULL) {
			if (ep->d_name[0] == '.')
				continue;
			
			NetNS_ptr entry(new NetNS());
			entry->name = ep->d_name;
			
			entry->path = NETNS_PATH;
			entry->path += entry->name;
			
			this->namespaces.push_back(std::move(entry));
		}
		
		closedir(dp);
	}
	
	
	// Prepare each namespace
	for (unsigned int i = 0; i < this->namespaces.size(); i++) {
		int err = this->namespaces[i]->prepare();
		if (err != 0)
			return err;
	}
	
	this->current = this->namespaces[0].get();
	
	return 0;
}

std::vector<NetNS *> NetNSManager::getNamespaces() {
	std::vector<NetNS *> ret;
	
	for (unsigned int i = 0; i < this->namespaces.size(); i++) {
		ret.push_back(this->namespaces.at(i).get());
	}
	
	return ret;
}

NetNS *NetNSManager::getCurrentNamespace() {
	return this->current;
}

NetNS *NetNSManager::getNamespaceByName(std::string name) {
	for (unsigned int i = 0; i < this->namespaces.size(); i++) {
		if (this->namespaces[i]->name == name)
			return this->namespaces[i].get();
	}
	
	return nullptr;
}

int NetNSManager::enter(NetNS *ns) {
	if (ns == nullptr)
		return EINVAL;
	
	int err = ns->enter();
	if (err != 0) {
		return err;
	}
	
	this->current = ns;
	
	return 0;
}

int NetNSManager::leave() {
	return this->enter(this->getNamespaceByName(this->home_ns));
}
