#include "ifacemanager.hpp"

std::vector<IFace *> IFaceManager::getInterfaces() {
	std::vector<IFace *> ret;
	
	for (unsigned int i = 0; i < this->interfaces.size(); i++) {
		ret.push_back(this->interfaces.at(i).get());
	}
	
	return ret;
}


bool IFaceManager::containsInterface(IFace *iface) {
	for (unsigned int i = 0; i < this->interfaces.size(); i++) {
		if (this->interfaces.at(i).get() == iface)
			return true;
	}
	
	return false;
}

IFace *IFaceManager::getInterfaceByIndex(int idx) {
	for (unsigned int i = 0; i < this->interfaces.size(); i++) {
		if (this->interfaces.at(i).get()->index == idx)
			return this->interfaces.at(i).get();
	}
	
	return nullptr;
}

// All interfaces are fully populated here
// This is because, as of now, the kernel cannot filter individual interfaces
// from the dump.  Because of this, if each IFace object populated itself, it
// would require making the same RTNETLINK call and filtering out that IFace's
// specific information.  It is simpler and easier to populate the entire
// array in one call to RTNETLINK.  The kernel can only return info on EVERY
// interface, so it's a little pointless to separate things.
// Possible TODO: Separate into individual functions
int IFaceManager::populate() {
	int   sock   = 0;
	char *buffer = (char *)malloc(NL_RECV_LENGTH);
	
	// Open a socket to talk to the kernel
	sock = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
	if (sock == -1) {
		free(buffer);
		return errno;
	}
	
	
	// Create the request structure
	struct {
		struct nlmsghdr  hdr;
		struct ifinfomsg info;
	} packet;
	
	// Populate the request
	memset(&packet, 0, sizeof(packet));
	packet.hdr.nlmsg_len   = NLMSG_LENGTH(sizeof(struct ifinfomsg));
	// Place a request of type RTM_GETLINK, return the entire table
	packet.hdr.nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	packet.hdr.nlmsg_type  = RTM_GETLINK;
	packet.info.ifi_family = AF_UNSPEC;
	packet.info.ifi_type   = 1;
	
	// Send the request off to the kernel
	if (send(sock, &packet, packet.hdr.nlmsg_len, 0) == -1) {
		int err = errno;
		free(buffer);
		close(sock);
		return err;
	}
	
	// The response will likely not come back all at once
	// Therefore, we need a flag to signal when we've recv'd
	// the last packet.
	bool end_xfer = false;
	
	//FILE *output = fopen("out.bin", "w");
	
	while (!end_xfer) {
		// Get a chunk of the response
		int ret_len = recv(sock, buffer, NL_RECV_LENGTH, 0);
		
		//fwrite(buffer, 1, ret_len, output);
		
		struct nlmsghdr *ret_msg = (struct nlmsghdr *)buffer;
		
		// Loop over the data returned
		// It is returned as a table of tables
		// There can be multiple netlink messages in a response
		// Each message can contain multiple attributes
		while (NLMSG_OK(ret_msg, ret_len)) {
			//DEBUG("Type %d", ret_msg->nlmsg_type);
			if (ret_msg->nlmsg_type == NLMSG_DONE) {
				end_xfer = true;
				break;
			} else if (ret_msg->nlmsg_type == RTM_NEWLINK) {
				// Grab a response message
				struct ifinfomsg *if_info = (struct ifinfomsg *)NLMSG_DATA(ret_msg);
				struct rtattr    *ret_rta = IFLA_RTA(if_info);
				
				// Create an interface instance that we will populate
				IFace_ptr iface(new IFace());
				
				iface->type  = if_info->ifi_type;
				iface->index = if_info->ifi_index;
				iface->up    = (if_info->ifi_flags & IFF_UP) != 0;
				
				// Loop over each attribute
				// Attributes may, themselves, contain attributes
				int attr_len = IFLA_PAYLOAD(ret_msg);
				while (RTA_OK(ret_rta, attr_len)) {
					//DEBUG("IFLA %s %d", iface->name.c_str(), ret_rta->rta_type);
					switch (ret_rta->rta_type) {
						// This attribute contains the interface's name
						case IFLA_IFNAME: {
							iface->name = (char *)RTA_DATA(ret_rta);
							
							break;
						}
						
						// Does this interface have a master?
						case IFLA_MASTER: {
							int *x = (int *)RTA_DATA(ret_rta);
							iface->master = *x;
							break;
						}
						
						// Interface link index
						// Veth devices populate this field
						case IFLA_LINK: {
							int *p = (int *)RTA_DATA(ret_rta);
							
							// This check is needed because a non-veth
							// interface may return that attr
							// If the link and index are the same, ignore
							if (*p != iface->index)
								iface->peer = *p;
							
							break;
						}
						
						// This one is a nested attribute
						case IFLA_LINKINFO: {
							this->process_LINKINFO(iface.get(), ret_rta);
							
							break;
						}
					}
					
					ret_rta = RTA_NEXT(ret_rta, attr_len);
				}
				
				iface->typeCheck();
				this->interfaces.push_back(std::move(iface));
			}
			
			ret_msg = NLMSG_NEXT(ret_msg, ret_len);
		}
	}
	
	int err;
	if ((err = this->populateAddresses())) {
		close(sock);
		free(buffer);
		return err;
	}
	
	//fclose(output);
	
	// Housekeeping
	close(sock);
	free(buffer);
	
	return 0;
}



int IFaceManager::populateAddresses() {
	char *buffer = (char *)malloc(NL_RECV_LENGTH);
	int   sock   = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
	
	if (sock < 1) {
		free(buffer);
		return errno;
	}
	
	struct {
		struct nlmsghdr  hdr;
		struct ifaddrmsg addr;
	} packet;
	
	memset(&packet, 0, sizeof(packet));
	packet.hdr.nlmsg_len   = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	packet.hdr.nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	packet.hdr.nlmsg_type  = RTM_GETADDR;
	packet.addr.ifa_family = AF_UNSPEC;
	
	if (send(sock, &packet, packet.hdr.nlmsg_len, 0) == -1) {
		int err = errno;
		free(buffer);
		close(sock);
		return err;
	}
	
	bool end_xfer = false;
	while (!end_xfer) {
		// Get a chunk of the response
		int ret_len = recv(sock, buffer, NL_RECV_LENGTH, 0);
		
		//fwrite(buffer, 1, ret_len, output);
		
		struct nlmsghdr *ret_msg = (struct nlmsghdr *)buffer;
		
		// Loop over the data returned
		// It is returned as a table of tables
		// There can be multiple netlink messages in a response
		// Each message can contain multiple attributes
		while (NLMSG_OK(ret_msg, ret_len)) {
			//DEBUG("Type %d", ret_msg->nlmsg_type);
			if (ret_msg->nlmsg_type == NLMSG_DONE) {
				end_xfer = true;
				break;
			} else if (ret_msg->nlmsg_type == RTM_NEWADDR) {
				// Grab a response message
				struct ifaddrmsg *if_addr = (struct ifaddrmsg *)NLMSG_DATA(ret_msg);
				struct rtattr    *ret_rta = IFLA_RTA(if_addr);
				
				int attlen = IFLA_PAYLOAD(ret_msg);
				while (RTA_OK(ret_rta, attlen)) {
					if (ret_rta->rta_type == IFA_LOCAL) {
						IFace *iface = this->getInterfaceByIndex(if_addr->ifa_index);
						
						unsigned char     *addr   = (unsigned char *)RTA_DATA(ret_rta);
						std::stringstream  addr_s;
						
						addr_s << (unsigned int)addr[0] << "." << (unsigned int)addr[1] << "." << (unsigned int)addr[2] << "." << (unsigned int)addr[3];
						
						iface->addresses.push_back(addr_s.str());
					}
					
					ret_rta = RTA_NEXT(ret_rta, attlen);
				}
			}
			
			ret_msg = NLMSG_NEXT(ret_msg, ret_len);
		}
	}
	
	free(buffer);
	close(sock);
	
	return 0;
}



int IFaceManager::process_LINKINFO(IFace *iface, struct rtattr *rta) {
	struct rtattr *info_rta = (struct rtattr *)RTA_DATA(rta);
	int            info_len = RTA_PAYLOAD(rta);
	
	while (RTA_OK(info_rta, info_len)) {
		switch (info_rta->rta_type) {
			// What kind of link is it?
			case IFLA_INFO_KIND: {
				// First grab the name from the RTA structure
				iface->link_type_name = (char *)RTA_DATA(info_rta);
				
				// Try to figure out what it is from the name
				iface->link_type = IFace::getLinkType(iface->link_type_name);
				
				// If unknown, return unsupposed socket type
				if (iface->link_type == IFace::LINK_UNKNOWN) {
					return ESOCKTNOSUPPORT;
				}
				
				break;
			}
			
			case IFLA_INFO_DATA: {
				if (iface->link_type == IFace::LINK_BOND) {
					this->process_INFO_DATA_BOND(iface, info_rta);
				}
				break;
			}
			
			// If it's a slave to another interface, what kind?
			case IFLA_INFO_SLAVE_KIND: {
				iface->slave_type_name = (char *)RTA_DATA(info_rta);
				iface->slave_type      = IFace::getLinkType(iface->slave_type_name);
				
				// If unknown, return unsupposed socket type
				if (iface->link_type == IFace::LINK_UNKNOWN) {
					return ESOCKTNOSUPPORT;
				}
				
				break;
			}
			
			case IFLA_INFO_SLAVE_DATA: {
				if (iface->slave_type == IFace::LINK_BOND) {
					this->process_INFO_SLAVE_DATA_BOND(iface, info_rta);
				} else if (iface->slave_type == IFace::LINK_BRIDGE) {
					this->process_INFO_SLAVE_DATA_BRIDGE(iface, info_rta);
				} else {
					DEBUG("IFace %s, type %d", iface->name.c_str(), iface->slave_type);
				}
					
				break;
			}
			
			default: {
				DEBUG("%s: Caught data of type %d", iface->name.c_str(), info_rta->rta_type);
			}
		}
		
		info_rta = RTA_NEXT(info_rta, info_len);
	}
	
	return 0;
}



int IFaceManager::process_INFO_DATA_BOND(IFace *iface, struct rtattr *rta) {
	struct rtattr *info_rta = (struct rtattr *)RTA_DATA(rta);
	int            info_len = RTA_PAYLOAD(rta);
	
	while (RTA_OK(info_rta, info_len)) {
		switch (info_rta->rta_type) {
			default: {
				//DEBUG("BOND %s: Caught data of type %d", iface->name.c_str(), info_rta->rta_type);
			}
		}
		
		info_rta = RTA_NEXT(info_rta, info_len);
	}
	
	return 0;
}


int IFaceManager::process_INFO_SLAVE_DATA_BRIDGE(IFace *iface, struct rtattr *rta) {
	struct rtattr *info_rta = (struct rtattr *)RTA_DATA(rta);
	int            info_len = RTA_PAYLOAD(rta);
	
	while (RTA_OK(info_rta, info_len)) {
		switch (info_rta->rta_type) {
			default: {
				//DEBUG("BRIDGE %s: Caught data of type %d", iface->name.c_str(), info_rta->rta_type);
			}
		}
		
		info_rta = RTA_NEXT(info_rta, info_len);
	}
	
	return 0;
}



int IFaceManager::process_INFO_SLAVE_DATA_BOND(IFace *iface, struct rtattr *rta) {
	struct rtattr *info_rta = (struct rtattr *)RTA_DATA(rta);
	int            info_len = RTA_PAYLOAD(rta);
	
	while (RTA_OK(info_rta, info_len)) {
		switch (info_rta->rta_type) {
			default: {
				//DEBUG("BOND SLAVE %s: Caught data of type %d", iface->name.c_str(), info_rta->rta_type);
			}
		}
		
		info_rta = RTA_NEXT(info_rta, info_len);
	}
	
	return 0;
}
