CC     =g++
CFLAGS =-c -Wall -std=c++11 -g -D_DEBUG -Wno-format-extra-args
LDFLAGS=-g
SOURCES=netns.cpp netnsmanager.cpp iface.cpp ifacemanager.cpp viewer.cpp systemmanager.cpp
OBJECTS=$(SOURCES:.cpp=.o)
OUTPUT =viewer

all: $(SOURCES) $(OUTPUT)

$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o
	rm $(OUTPUT)
	rm .depends

depends: .depends
.depends: $(SOURCES)
	rm -f .depends
	@$(CC) $(CFLAGS) -MM $^>> .depends

-include .depends
