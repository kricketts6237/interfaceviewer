#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <cstring>
#include <iomanip>

#include "systemmanager.hpp"

int main(void) {
	if (getuid() != 0) {
		std::cout << "Run with sudo" << std::endl;
		return 1;
	}
	
	SystemManager sysman;
	
	int err = sysman.populate();
	if (err) {
		std::cout << "Error: " << strerror(err) << std::endl;
		return 1;
	}
	
	
	std::vector<NetNS *> ns_list = sysman.getNamespaces();
	for (NetNS *ns : ns_list) {
		std::cout << "Namespace: " << ns->name << std::endl;
		std::cout << std::left
		          << std::setw(3)  << "#"
		          << std::setw(12) << "Name"
		          << std::setw(6)  << "U/D"
		          << std::setw(12) << "Type"
		          << std::setw(12) << "Master"
		          << std::setw(20) << "Peer"
		          << std::setw(24) << "Addresses"
		          << std::endl;
		
		std::vector<IFace *> ifaces;
		if ((err = sysman.getInterfacesInNamespace(ns, &ifaces))) {
			std::cout << "Could not retrieve interfaces for namespace " << ns->name << std::endl;
			return 1;
		}
		
		for (IFace *iface : ifaces) {
			int peerIdx   = iface->peer;
			int masterIdx = iface->master;
			
			std::string peerName = "-";
			if (peerIdx != -1) {
				IFace *peer    = sysman.getInterfaceByIndex(peerIdx);
				NetNS *peer_ns = sysman.getNamespaceForInterface(peer);
				
				peerName = peer->name;
				if (ns != peer_ns)
					peerName += "@" + peer_ns->name;
			}
			
			std::cout << std::left
		              << std::setw(3)  << iface->index
		              << std::setw(12) << iface->name
		              << std::setw(6)  << (iface->up ? "UP" : "DOWN")
		              << std::setw(12) << iface->link_type_name
		              << std::setw(12) << (masterIdx == -1 ? "-" : sysman.getInterfaceByIndex(masterIdx)->name)
		              << std::setw(20) << peerName;
			
			if (iface->addresses.size() != 0) {
				for (std::string addr : iface->addresses) {
					std::cout << addr << " ";
				}
			} else {
				std::cout << "-";
			}
			
			std::cout << std::endl;
		}
		
		std::cout << std::endl;
	}
	
	
	return 0;
}
