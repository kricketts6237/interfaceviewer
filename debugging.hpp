#ifndef DEBUGGING_HPP
#define DEBUGGING_HPP

#include <cstdio>

#define S(x) #x
#define S_(x) S(x)
#define S__LINE__ S_(__LINE__)

#ifdef _DEBUG
	#define DEBUG(fmt, ...) do {\
		printf("DEBUG [" __FILE__ ":" S__LINE__ "]: " fmt "\n", __VA_ARGS__);\
	} while (false);
	
	#define DEBUG_BLOCK(x) do {\
		x\
	} while (false);
#else
	#define DEBUG(fmt, ...) ;
	#define DEBUG_BLOCK(x) ;
#endif

#endif
