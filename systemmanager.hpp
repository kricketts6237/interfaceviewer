#ifndef SYSTEMMANAGER_HPP
#define SYSTEMMANAGER_HPP

#include <vector>
#include <map>

#include "netnsmanager.hpp"
#include "ifacemanager.hpp"
#include "debugging.hpp"

typedef std::unique_ptr<IFaceManager> IFaceManager_ptr;

class SystemManager {
	public:
		NetNSManager                            ns_manager;
		std::map<std::string, IFaceManager_ptr> iface_managers;
		
		int populate();
		
		NetNS *getNamespaceForInterface(IFace *iface);
		
		std::vector<NetNS *>         getNamespaces();
		std::vector<IFace *>         getInterfaces();
		std::vector<IFaceManager *>  getIFaceManagers();
		int                          getInterfacesInNamespace(NetNS *ns, std::vector<IFace *> *ret);
		IFace                       *getInterfaceByIndex(int idx);
};

#endif
